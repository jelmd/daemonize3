/**
 * Copyright (c) 2016 Jens Elkner. All rights reserved.
 * License: CDDL 1.0
 */

'use strict';

/**
 * This is the utility, which gets spawned by the runner. It talks to the
 * parent process ('runner.js') via 'ipc' and after setting up required stuff,
 * it runs the "main" application. When the application itself has made all
 * preparations to run and has startet its main handler, e.g. like
 * http.server.listen($port, $cb), it should signal its state by emitting a
 * 'daemon_running' event (here $cb would be a good place to do it). The
 * corresponding listener will catch the event, propagate it to its parent and
 * cleanup related stuff, i.e. disconnect the corresponding channel[s] and
 * clear scheduled timeout handlers installed via this snippet, so that the app
 * is able to exit as usual.
 *
 * If the "main" application does not emit a 'daemon_running' event, a timer
 * will fire after options.timeout seconds, unless it is not set or 0. It is
 * basically treated as an "I'm alive" message by the parent aka runner, which
 * in turn does than the cleanup on his side to be able to exit.
 *
 * If the parent doesn't receive a "daemon_{ok|exit}" feedback from this daemon
 * within options.timeout + 1 seconds, it assumes, that the child is dead,
 * hangs or got killed silently and thus will set its own exit code to 1 and
 * cleanup its side.
 *
 * The daemon also listens for the 'exit' event. If it occurs, it sends a 
 * corresponding message with the process.exitCode to the parent, which in turn
 * will exit with this code as well. So if the "main" application wants to
 * notify the user about certain problems by exiting with a certain exit code,
 * it should make sure, that it got finally set to the appropriate value.
 */

/** Catch and propagate SW errors to the parent during the preparation phase. */
process.uceHandler = function(err) {
	process.exitCode = 127;
	var o = { info: 'sw_error',  code: 127, msg: err.message };
	if (process.connected) {
		process.send(o);
	} else if (!!process.__socket) {
		process.__socket.write(JSON.stringify(o));
	}
	process.exit();
};
process.once('uncaughtException', process.uceHandler);

/** Application is running fine: notify the parent and cleanup. */
process.once('daemon_running', function(message) {
	if (!!process.__timeoutId)
		try { clearTimeout(process.__timeoutId); } catch (e) { };
	if (!message)
		message = '';
	var o = { info: 'daemon_ok', code: process.exitCode, msg: message };
	if (process.connected) {
		process.send(o, function() {
			if (process.connected) process.disconnect();
		});
		o = null;
	}
	if (!!process.__socket) {
		var socket = process.__socket;
		delete process.__socket;
		if (o)
			socket.write(JSON.stringify(o));
		socket.end();
	}
});

/** Somewhere process.exit() wa called. Provide feedback to the parent and
	cleanup. */
process.once('exit', function() {
	if (process.connected) {
		process.disconnect();
	}
	if (!!process.__socket) {
		try { process.__socket.end(); } catch (e) { }
	}
});

/** Wait 'til the parent send us the 'start_daemon' message, which tells us,
	that it is ready and listens for feedback (ignore all other messages).
	If 'start_daemon' has been received, setup an add. feedback channel (socket)
	and timers, and finally run the app. When it returns, IPC channel gets
	disconnected (if not already done), so that the app may exit as usual.
	If the app provides feedback later (since async), the alternative channel
	will be used to feed it to the parent. */
process.on('message', function(message) {
	process.__timeoutId = null;

	if (message.cmd != 'start_daemon')
		return;

	// only once, avoid interference with the "main" app
	var x = (() => this);
	process.removeListener('message', x);

	if (message.options === undefined) {
		throw new Error("daemon options missing");
	}
	var opts = message.options;

	// check for the "main" app script to run
	var main = '';
	if (!opts.main) {
		throw new Error("daemon options:main missing");
	} else {
		try {
			main = require.resolve(opts.main);
			opts.main = main;
		} catch (e) {
			// handled later
		}
	}
	if (main.length == 0)
		throw new Error('Unable to resolve app script "' + opts.main + '"');

    // convinience: set process specific parameters, if available
	if ((!!opts.umask) && opts.umask.length > 0) {
		try {
			process.umask(opts.umask);
		} catch (e) {
			throw new Error('failed to set umask "'+ opts.umask + '"');
		}
	}

	var t = 0;
	if (!!opts.timeout) {
		try {
			t = parseInt(opts.timeout) * 1000;
		} catch (e) {
			throw new Error('Invalid timeout value "' + opts.timeout + '"');
		}
		if (t < 0)
			throw new Error('Timeout value "'+ opts.timeout +'" out of range');
	}
	if (t > 0) {
		process.__timeoutId = setTimeout(function() {
			delete process.__timeoutId;
			process.emit('daemon_running',
				'Timeout reached - assuming daemon is running');
		}, t);
		process.__timeoutId.unref();
	}
	if (!!opts.socket) {
		var socket = require('net')
			.connect({ path: opts.socket, allowHalfOpen: true});
		process.__socket = socket;
		socket.unref();
	}
	// this doesn't get reflected to the OS, however, node internally it
	// overwrites the executed script name - "main" app may rely on it.
    process.argv[1] = main;
	
	// the app may have its own UCE listener and recover or just relies on the
	// fact, that non is installed. So if we would leave it active, 
	process.removeListener('uncaughtException', process.uceHandler);
	delete process.uceHandler;
	process.exitCode = 0;

	// run the "main" app
	var app = require(main);
	if (typeof app == 'function')
		app();

	// When we disconnect here, even via nextTick etc., the chances are high,
	// that the IPC channel gets closed before the app is able to notify
	// its parent => would need to wait for the timeout handler invocation
	// (same thing, if the app doesn't emit a 'daemon_running' event at all).
	// On the other side, if the app has an error or is finished,
	// exit doesn't get called because the IPC channel is still => node will not
	// allow it to die => would need to wait for the timeout handler trigger
	// again :(.
	// Therefore we use our own unref'ed socket above as fallback to be able
	// notify the parent, let the daemon disconnect the IPC channel, when
	// it is possible and thus being able to exit immediately if the app either
	// emits a 'daemon_running' event or is ready to exits as usual. 
	setImmediate(function() { if (process.connected) process.disconnect(); });
});
